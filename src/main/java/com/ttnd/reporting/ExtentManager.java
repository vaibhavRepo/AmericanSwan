package com.ttnd.reporting;

import com.relevantcodes.extentreports.ExtentReports;
import com.ttnd.utilities.GlobalVar;

public class ExtentManager {

private static ExtentReports extent;
    
    public static ExtentReports getInstance(String device) {
        if (extent == null) {
            extent = new ExtentReports(GlobalVar.REPORT_FOLDER_FOR_ENVIRONMENT + device +".html", true);
            
            // optional
            extent.config()
                .documentTitle("MTAF Automation Report")
                .reportName("Environment " + GlobalVar.ENVIRONMENT)
                .reportHeadline("\tDevice: " + device);
               
            // optional
            extent
                .addSystemInfo("Selenium Version", "2.46")
                .addSystemInfo("Appium Version", "1.4.0")
                .addSystemInfo("Environment", GlobalVar.ENVIRONMENT);
        }
        return extent;
    }
}
