package com.ttnd.reusableComponent;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.objectRepository.HomePage;
import com.ttnd.objectRepository.Page;
import com.ttnd.objectRepository.SignInPage;
import com.ttnd.testScripts.TestBase;
import com.ttnd.utilities.GlobalVar;
import com.ttnd.utilities.Keywords;


public class ReusableFunctions extends TestBase{


    /*
     * Navigate to base URl
     */
	public void navigateToURL(){
		getDriver().navigate().to(GlobalVar.baseURL);
		reportTestStep(true, "navigated to website");

	}
	
	/*
	 * Generate Random 10 digit mobile number for testdata
	 */
	public String getMobile(){
		/*Random rand = new Random();

		int  n = rand.nextInt(99) + 10;
		int  n1 = rand.nextInt(99) + 10;
		int  n2 = rand.nextInt(99) + 10;

		return(n1+""+n2+""+n+""+n1+""+n);*/
		long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
		return(String.valueOf(number));
	}
	
	/*
	 * Generate Random email for email
	 */
	public String getEmail(){
		Random rand = new Random();

		int  n = rand.nextInt(99) + 10;
		int  n1 = rand.nextInt(99) + 10;
		int  n2 = rand.nextInt(99) + 10;

		return(n1+""+n2+""+n+""+n1+"@yopmail.com");
	}

	/*
	 * This function will navigate home screen during starting of each test
	 */
	public HomePage loginToAccount(){

		navigateToURL();
		if(getDriver().findElements(By.xpath("//*[@id='inputEmail']")).size()>0){
			SignInPage signIn = new SignInPage(getDriver());
			signIn.enterEmail(GlobalVar.email);
			signIn.enterPassword(GlobalVar.password);
			return signIn.clickLoginButton();
		}
		else{	
			return new HomePage(getDriver());

		}
	}

	


}
