package com.ttnd.utilities;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ttnd.reusableComponent.ReusableFunctions;
import com.ttnd.testScripts.TestBase;

public class Keywords {


	public  void WaitForElement(RemoteWebDriver driver, By element) throws NoSuchElementException{
		try{
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(element));
			//		wait.until(ExpectedConditions.presenceOfElementLocated(titleLocator));
		}	catch(Exception e){
			TestBase.reportTestStep(false, "Presence of element no found : " + e);

			throw new FrameworkException();

		}	
	}

	TestBase b = new TestBase();
	// Overloaded WaitForElement
	 
	public  void WaitForElement(RemoteWebDriver driver, By titleLocator, String message) {
		WaitForElement(driver, titleLocator);
		System.out.println(message);
	}



	public  boolean isElementFound(RemoteWebDriver driver, By element) throws NoSuchElementException{
		try{
			if(!driver.findElements(element).isEmpty()) {
				return true;
			}else{
				return false;
			}
		}	catch(Exception e){
			TestBase.reportTestStep(false, "Cannot find element : " + e);

			throw new FrameworkException();

		}	

	}


	public  void back(RemoteWebDriver driver) {
		//driver.navigate().back();
		
		TestBase.reportTestStep(false, "navigate back");
	}


	public  void clickinElementsByText(RemoteWebDriver driver, By ElementsID, String ElementText){
		boolean noElementFound = false;
		try{
			List<WebElement> elements = driver.findElements(ElementsID);
			for(WebElement element : elements){
				if(element.getAttribute("text").equals(ElementText)){
					noElementFound = true;
					element.click();
					break;
				}
			}
			if(noElementFound==false){
				throw new FrameworkException("No element found : " + ElementText);
			}
		}
		catch(NoSuchElementException e){
			throw e;
		}
		catch(Exception e){
			throw e;
		}
	}

	public  void clickElement (WebElement element)
	{
		try
		{
			element.click();
		}
		catch(Exception e){
			TestBase.reportTestStep(false, "Element cannot be clicked : " + e);

			throw new FrameworkException();

		}	
	}

	public   WebElement findElement(RemoteWebDriver driver ,By locator ,int timeoutInSeconds )
	{
		WebElement element = null;
		WebDriverWait wait;
		try
		{
			if (timeoutInSeconds > 0)
			{
				wait = new WebDriverWait(driver,timeoutInSeconds);
				element = (WebElement) wait.until(
						ExpectedConditions.presenceOfElementLocated(locator));                     	
				element = driver.findElement(locator);
			}
			else{
				element = driver.findElement(locator);
			}
			return element;
		}
		catch(Exception e){
			TestBase.reportTestStep(false, "Cannot find element : " + e.getMessage());

			throw new FrameworkException();

		}	
	}
	
	
	public   WebElement findinElementsbyText(RemoteWebDriver driver ,By locator , String text, int timeoutInSeconds )
	{
		List<WebElement> mobileElement = null;
		WebDriverWait wait;
		try
		{
			if (timeoutInSeconds > 0)
			{
				wait = new WebDriverWait(driver,timeoutInSeconds);
				mobileElement = wait.until(
						ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
				
				mobileElement = driver.findElements(locator);
			}
			else{
				mobileElement = driver.findElements(locator);
			}
			
			
			for(WebElement element : mobileElement){
				if(GetElementAttributeValue((WebElement)element, "text").equalsIgnoreCase(text)){
					TestBase.reportTestStep(true, "Successfully found Element : " + text);

					return (WebElement) element;
				}
			}
			TestBase.reportTestStep(false, "Category not in List ");

			return null;

		}catch(Exception e){
			TestBase.reportTestStep(false, "Cannot find element : " + e);

			throw new FrameworkException();

		}
	}
	
		
		public   int findElements(RemoteWebDriver driver ,By locator , int timeoutInSeconds )
		{
			List<WebElement> mobileElement = null;
			WebDriverWait wait;
			try
			{
				if (timeoutInSeconds > 0)
				{
					wait = new WebDriverWait(driver,timeoutInSeconds);
					wait.until(
							ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
					
					mobileElement = driver.findElements(locator);
				}
				else{
					mobileElement = driver.findElements(locator);
				}
				
			
				TestBase.reportTestStep(true, "Elements found ");
                   return mobileElement.size();


			}
		catch(Exception e){
			TestBase.reportTestStep(false, "Cannot find element : " + e);

			throw new FrameworkException();

		}	
	}
		
		
		public   List<WebElement> findAllElements(RemoteWebDriver driver ,By locator , int timeoutInSeconds )
		{
			List<WebElement> mobileElement = null;
			WebDriverWait wait;
			try
			{
				if (timeoutInSeconds > 0)
				{
					wait = new WebDriverWait(driver,timeoutInSeconds);
					wait.until(
							ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
					
					mobileElement = driver.findElements(locator);
				}
				else{
					mobileElement = driver.findElements(locator);
				}
				
			
                   return(mobileElement);


			}
		catch(Exception e){
			TestBase.reportTestStep(false, "Cannot find element : " + e);

			throw new FrameworkException();

		}	
	}
		

	public  void sendKeys(WebElement mobileElement, String value){
		try{
			mobileElement.clear();
			mobileElement.sendKeys(value);
		}catch(Exception e){
			throw e;
		}

	}


	public  String GetElementAttributeValue(WebElement mobileElement, String attribute)
	{
		try
		{
			if(attribute.equalsIgnoreCase("text"))
			  return mobileElement.getAttribute(attribute);
			else if(attribute.equalsIgnoreCase("onScreen"))
				  return String.valueOf(mobileElement.getAttribute(attribute));
		}catch(Exception e){
			TestBase.reportTestStep(false, "Cannot get text attribute : " + e);

			throw new FrameworkException();	
			}
		return null;

	}
}
