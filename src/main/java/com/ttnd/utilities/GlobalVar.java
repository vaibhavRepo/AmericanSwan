package com.ttnd.utilities;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class GlobalVar {

	public static  Xls_Reader xls = new Xls_Reader(System.getProperty("user.dir")+"\\src\\main\\resources\\TestData.xlsx");

	
	public static final String ENVIRONMENT="UAT";
	//Available: QA, UAT, Prod
	
	public static final String APK_PATH = System.getProperty("user.dir") + "\\src\\main\\resources";
	
	public static final String baseURL = "http://AS-Grails:AS1-D3f@u|t@admin.qa.americanswan.com/";
	public static final String email = "vaibhav.singhal@tothenew.com";
	public static final String password = "@Password123";

	
	public static final String REPORT_FOLDER_DRIVE = "C:\\";
	public static final String REPORT_FOLDER_ROOT_NAME = "AS_Reports";
	public static final String CONSOLIDATED_REPORT_FILE_NAME = "index.html";

	public static final String RESOURCE_TEST_DATA_FOLDER = System.getProperty("user.dir") + "\\src\\test\\resources\\TestData\\";
	public static final String REPORT_RESOURCES_FOLDER = System.getProperty("user.dir") + "\\src\\test\\resources\\resources";
	public static final String REPORT_FOLDER_FOR_ENVIRONMENT = REPORT_FOLDER_DRIVE + REPORT_FOLDER_ROOT_NAME + "\\" + ENVIRONMENT + "\\";

	public static final String CURRENT_EXECUTION_MODE = "";

	public static final String TESTSUITE_FILE_NAME= "TestSuite";
	public static final String TESTSUITE_FILE_EXTENSION= ".xls";
	
	//Not final variables
	public static Map<String,String> TEST_DATA;
}
