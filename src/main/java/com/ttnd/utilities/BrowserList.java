package com.ttnd.utilities;

import java.util.ArrayList;
import java.util.List;

public class BrowserList {

	List<String> browserList;
	private static int i;
	private static BrowserList instance;

	private BrowserList() {
		browserList = new ArrayList<>();
		browserList.add("firefox");
		browserList.add("chrome");
	}

	public static synchronized BrowserList getInstance(){
		if(instance == null){
			instance = new BrowserList();
		}return instance;
	}

	public List<String> getBrowserList() {
		return browserList;
	}

	public String getBrowser() {
		System.out.println(browserList.size());
		String strDevice = browserList.get(0);
//		deviceList.remove(0);
		System.out.println(strDevice);
		return strDevice;
			
	}
}
