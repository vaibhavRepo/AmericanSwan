package com.ttnd.testScripts;

public class CategoryList {


	String[][] arr = new String[14][3];
	
	public String[][] getCategory(){
		//arr[0][0] ="MEN";arr[0][1] ="Topwear";arr[0][2]="Shirts";
		arr[1][0] ="MEN";arr[1][1] ="Topwear";arr[1][2]="T-Shirts";
		arr[2][0] ="MEN";arr[2][1] ="Topwear";arr[2][2]="Polos";
		arr[3][0] ="MEN";arr[3][1] ="Topwear";arr[3][2]="Pullovers";
		arr[4][0] ="MEN";arr[4][1] ="Topwear";arr[4][2]="Sweat Shirts";
		arr[5][0] ="MEN";arr[5][1] ="Topwear";arr[5][2]="Blazers";
		arr[6][0] ="MEN";arr[6][1] ="Topwear";arr[6][2]="Jackets";
		arr[7][0] ="WOMEN";arr[7][1] ="Bottomwear";arr[7][2]="Trouser";
		arr[8][0] ="WOMEN";arr[8][1] ="Bottomwear";arr[8][2]="Pants";
		arr[9][0] ="WOMEN";arr[9][1] ="Bottomwear";arr[9][2]="Denim";
		arr[10][0] ="WOMEN";arr[10][1] ="Bottomwear";arr[10][2]="Leggings & Stocking";
		arr[11][0] ="WOMEN";arr[11][1] ="Bottomwear";arr[11][2]="Track Pants";
		arr[12][0] ="WOMEN";arr[12][1] ="Bottomwear";arr[12][2]="Shorts/Skirt";
		arr[13][0] ="WOMEN";arr[13][1] ="Bottomwear";arr[13][2]="Sweat Pants";
	
		
		return arr;

	}
}
