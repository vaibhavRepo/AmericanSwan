package com.ttnd.testScripts;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.ttnd.reporting.Reporting;
import com.ttnd.reusableComponent.ReusableFunctions;
import com.ttnd.utilities.GlobalVar;
import com.ttnd.utilities.Keywords;

import java.awt.Desktop;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {
	/**
	 * WebDriver
	 */
	public ThreadLocal<RemoteWebDriver> threaddriver;
	public String URL; 
	/**
	 * Reporting
	 */
	protected ThreadLocal<ExtentReports> extent = new ThreadLocal<ExtentReports>();
	protected static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
	protected ThreadLocal<String> reportName = new ThreadLocal<String>();
	protected static String reportsDirEnv;
	protected static ArrayList<String> browserList = new ArrayList<String>();



	protected Properties properties;

	public TestBase(){
		properties = new Properties();
		try {
			FileReader reader = new FileReader("master_config.properties");
			properties.load(reader);


		} 
		catch (IOException e) {
			System.out.println("Failed to load Properties file");
		}
	}

	@BeforeSuite
	public void beforeSuite(){
		reportsDirEnv = Reporting.createResultFolderStructure(GlobalVar.ENVIRONMENT);
		System.out.println(reportsDirEnv);
	}

	@BeforeTest
	@Parameters("browser")
	public void beforeTest(String browser){
		reportName.set(setupBrowserReports(reportsDirEnv, browser));
		extent.set(new ExtentReports(reportName.get()+ "\\"+ "Execution_Report" +".html",true));


		try{

			if (browser.equalsIgnoreCase("firefox_v43") || 
					browser.equalsIgnoreCase("firefox_v41"))
			{

				System.out.println(" Executing on FireFox");
				String Node = "http://10.1.0.149:4444/wd/hub";

				DesiredCapabilities cap = DesiredCapabilities.firefox();
				cap.setBrowserName("firefox");
				cap.setPlatform(Platform.WINDOWS);
				if(browser.equalsIgnoreCase("firefox_v43"))
					cap.setVersion("43");
				else if(browser.equalsIgnoreCase("firefox_v41"))
					cap.setVersion("41");
				threaddriver = new ThreadLocal<RemoteWebDriver>();
				threaddriver.set(new RemoteWebDriver(new URL(Node), cap));


				// Puts an Implicit wait, Will wait for 10 seconds before throwing exception
				getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				getDriver().manage().window().maximize();

				browserList.add(browser);

			}
			else if (browser.contains("chrome_v47"))
			{
				System.out.println(" Executing on CHROME");

				String Node = "http://10.1.0.149:4444/wd/hub";
				System.setProperty("webdriver.chrome.driver", "D:\\TehnologyWork\\Softwares\\chromedriver.exe");

				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setBrowserName("chrome");
				cap.setPlatform(Platform.WINDOWS);
				cap.setVersion("47");

				threaddriver = new ThreadLocal<RemoteWebDriver>();
				threaddriver.set(new RemoteWebDriver(new URL(Node), cap));


				// Puts an Implicit wait, Will wait for 10 seconds before throwing exception
				getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				getDriver().manage().window().maximize();

				browserList.add(browser);


			}

		}
		catch (Exception e) {
			System.out.println(e);
		}

	}



	@AfterTest
	public void afterTest(){
		File htmlFile = new File(reportName.get()+"\\ExecutionReport.html");
		extent.get().close();

		getDriver().quit();

		
	}

	public static void reportTestStep(boolean status, String message) {

		if(status){
			test.get().log(LogStatus.PASS, message);
		}else{
			test.get().log(LogStatus.FAIL, message);
			System.err.println(message);
		}
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser, Method method) throws Exception {

		
		
		System.out.println("Executing " +method.getName());
		System.out.println(reportName.get());
		test.set(extent.get().startTest(this.getClass().getSimpleName() +" "+  method.getName(),method.getName()));


	}

	public RemoteWebDriver getDriver() {
		return threaddriver.get();
	}


	@AfterMethod
	public void tearDown() throws Exception {
		extent.get().endTest(test.get());

		extent.get().flush();
	}

	@AfterSuite
	public void afterSuite(){
		


		new Reporting().generateFinalReport(browserList);

		System.out.println(reportsDirEnv+ "//index.html");
		File htmlFile = new File(reportsDirEnv+ "//index.html");


		try {
			Desktop.getDesktop().browse(htmlFile.toURI());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String setupBrowserReports(String reportsDir, String browserName) {
		File reportTemp = new File(reportsDir, browserName);
		if(reportTemp.exists()){
			reportTemp.mkdirs();
		}
		return reportTemp.getAbsolutePath();
	}	
}