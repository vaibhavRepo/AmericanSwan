package com.ttnd.testScripts;

import java.util.Hashtable;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ttnd.objectRepository.AddCustomerPage;
import com.ttnd.objectRepository.BlogPage;
import com.ttnd.objectRepository.CarouselPage;
import com.ttnd.objectRepository.CustomersPage;
import com.ttnd.objectRepository.HomePage;
import com.ttnd.objectRepository.MediaPage;
import com.ttnd.objectRepository.NewArrivals;
import com.ttnd.reusableComponent.ReusableFunctions;
import com.ttnd.utilities.GlobalVar;
import com.ttnd.utilities.TestUtil;

public class Admin_Tests extends ReusableFunctions{

	@DataProvider()
	public Object[][] getLoginDataMedia(){
		return TestUtil.getData("addAsSeenIn", GlobalVar.xls);
	}
	
	/*
	 * Added new As Seen In Post and verifying same
	 */
	@Test(dataProvider="getLoginDataMedia")
	public void addAsSeenIn(Hashtable<String,String> data) {
		HomePage homePage = loginToAccount();
		homePage.clickonCMS();
		MediaPage mediaPage = homePage.clickonMedia();

		int initialItems = mediaPage.totalAsSeenIn();
		mediaPage.clickonAdd();
		mediaPage.enterNameToNew(data.get("NewName"));
		mediaPage.enterPostedOnToNew(data.get("Date"));
		mediaPage.uploadDefaultImageToLastItem();
		mediaPage.clickonSave();
		int finalItems = mediaPage.totalAsSeenIn();

		if(finalItems==(initialItems+1))
			reportTestStep(true, "As Seen In Post is successfully addded");
		else
			reportTestStep(false, "As Seen In Post is NOT successfully addded");

	}
	
	@DataProvider()
	public Object[][] getLoginDataCustomer(){
		return TestUtil.getData("addNewCustomer", GlobalVar.xls);
	}
	
	/*
	 * Add new Customer and verify if new Customer Account is created
	 */
	@Test(dataProvider="getLoginDataCustomer")
	public void addNewCustomer(Hashtable<String,String> data) {
		HomePage homePage = loginToAccount();
		String email = getEmail();
		homePage.clickonManage();
		CustomersPage customerPage = homePage.clickonCustomers();
		AddCustomerPage addcustomerPage = customerPage.addCustomer();
		addcustomerPage.addFirstName(data.get("FirstName"));
		addcustomerPage.addEmail(email);
		addcustomerPage.addPassword(data.get("Password"));
		addcustomerPage.addMobile(getMobile());
		customerPage = addcustomerPage.clickSave();
		customerPage.clickFilter();
		customerPage.addEmailName(email);
		customerPage.clickSeach();
	
		if(customerPage.isEmailRegistered(email))
			reportTestStep(true, "New Customer is successfully created");
		else
			reportTestStep(false, "New Customer is NOT successfully created");

	}
	
	@DataProvider()
	public Object[][] addCustomerAddress(){
		return TestUtil.getData("addCustomersAddress", GlobalVar.xls);
	}
	
	/*
	 * Admin has option to add customer address
	 */
	@Test(dataProvider="addCustomerAddress")
	public void addCustomersAddress(Hashtable<String,String> data) {
		HomePage homePage = loginToAccount();
		String email = getEmail();
		String mobile = getMobile();
		
		homePage.clickonManage();
		
		CustomersPage customerPage = homePage.clickonCustomers();
		AddCustomerPage addcustomerPage = customerPage.addCustomer();
		addcustomerPage.addFirstName(data.get("FirstName"));
		addcustomerPage.addEmail(email);
		addcustomerPage.addPassword(data.get("Password"));
		addcustomerPage.addMobile(getMobile());
		customerPage = addcustomerPage.clickSave();
		customerPage.clickFilter();
		customerPage.addEmailName(email);
		customerPage.clickSeach();
		customerPage.clickAllCheckbox();
		customerPage.clickeditCustomerDetails();
		customerPage.clickAddressCustomerDetails();
		customerPage.addCustomerAddress(data.get("FirstName"),
				data.get("LastName"),
				mobile, 
				data.get("Address"),
				data.get("Pincode"));
		
		if(customerPage.verifyAddress())
			reportTestStep(true, "Admin successfully created Customer Address");
		else
			reportTestStep(false, "Admin was NOT successful in creating Customer Address");
	}
	
	@DataProvider()
	public Object[][] getLoginData(){
		return TestUtil.getData("addToCarousel", GlobalVar.xls);
	}
	
	
	/*
	 * Test will add an item in Carousel and will verify if it was successfuly added or not
	 */
	@Test(dataProvider="getLoginData")
	public void addToCarousel(Hashtable<String,String> data) {

		HomePage homePage = loginToAccount();
		homePage.clickonCMS();
		homePage.clickonHomePage();
		CarouselPage carousel = homePage.clickonCaeousal();
		int initialItems = carousel.totalItemIndex();
		carousel.clickonAddImage();
		carousel.enterRedirectUrlToLastItem(data.get("redirectURL"));
		carousel.selectDefaultImageToLastItem();
		carousel.uploadDefaultImageToLastItem();
		carousel.saveItem();
		int finalItems = carousel.totalItemIndex();

		if(finalItems==(initialItems+1))
			reportTestStep(true, "Item is successfully addded in Carousel");
		else
			reportTestStep(false, "Item is NOT successfully addded in Carousel");

	}


	/*
	 * Verify minimum 5 Products must be available in "MEN" category
	 */
	@Test
	public void verifyMen_MinimumNewArrivals() {

		HomePage homePage = loginToAccount();
		homePage.clickonCMS();
		homePage.clickonHomePage();
		NewArrivals newArrivals = homePage.clickonNewArrivals();
		newArrivals.clickMenTab();
		newArrivals.expandMenProducts();
		//int totalProducts = newArrivals.gettotalProductsPresent();
		//reportTestStep(true, "Total Products available in Men Product of New Arrival " + totalProducts);

		newArrivals.deleteAllMenProductsPresent();
		int finalProducts = newArrivals.gettotalProductsPresent();

		if(finalProducts==5)
			reportTestStep(true, "Minimum 5 Products needs to be present in Men New Arrival");
		else
			reportTestStep(false, "Products can be less than 5 in Men New Arival");
	}


	/*
	 * Verify minimum 5 Products must be available in "WOMEN" category
	 */
	@Test
	public void verifyWomen_MinimumNewArrivals() {
		HomePage homePage = loginToAccount();
		homePage.clickonCMS();
		homePage.clickonHomePage();
		NewArrivals newArrivals = homePage.clickonNewArrivals();
		newArrivals.clickWomenTab();
		newArrivals.expandWomenProducts();
		//int totalProducts = newArrivals.gettotalProductsPresent();
		//reportTestStep(true, "Total Products available in Men Product of New Arrival " + totalProducts);

		newArrivals.deleteAllWomenProductsPresent();
		int finalProducts = newArrivals.gettotalProductsPresent();

		if(finalProducts==5)
			reportTestStep(true, "Minimum 5 Products needs to be present in Women New Arrival");
		else
			reportTestStep(false, "Products can be less than 5 in Women New Arival");
	}


	/*
	 * Verify minimum 5 Products must be available in "DEFAULT" category
	 */
	@Test
	public void verifyDefault_MinimumNewArrivals() {
		HomePage homePage = loginToAccount();
		homePage.clickonCMS();
		homePage.clickonHomePage();
		NewArrivals newArrivals = homePage.clickonNewArrivals();
		newArrivals.clickDefaultTab();
		newArrivals.expandDefaultProducts();
		//int totalProducts = newArrivals.gettotalProductsPresent();
		//reportTestStep(true, "Total Products available in Men Product of New Arrival " + totalProducts);

		newArrivals.deleteAllDefaultProductsPresent();
		int finalProducts = newArrivals.gettotalProductsPresent();

		if(finalProducts==5)
			reportTestStep(true, "Minimum 5 Products needs to be present in Default New Arrival");
		else
			reportTestStep(false, "Products can be less than 5 in Default New Arival");
	}

	/*
	 * Add new Blog in Home Page Blogs
	 */
	@Test
	public void addNewBlog() {
		HomePage homePage = loginToAccount();
		homePage.clickonCMS();
		homePage.clickonHomePage();
		BlogPage blog = homePage.clickonBlogs();
		int initialItems = blog.totalBlogIndex();
		blog.clickonAddImage();
		blog.addDescriptionToLastBlog();
		blog.addRedirectUrlToLastBlog();
		blog.uploadDefaultImageToLastBlog();
		blog.saveItem();
		int finalItems = blog.totalBlogIndex();

		if(finalItems==(initialItems+1))
			reportTestStep(true, "Blog is successfully addded in Blog Page");
		else
			reportTestStep(false, "Blog is NOT successfully addded in Blog Page");
	}


	@DataProvider()
	public Object[][] getCarouselData(){
		return TestUtil.getData("addToCarouselSchedular", GlobalVar.xls);
	}
	
	/*
	 * Items aren't clickable, needs to be removed from read only by dev team to work.
	 */
	/*
	 * Test will add an item in Carousel and schedule  Schedular and will verify if it was successfully created or not
	 */
	/*@Test(dataProvider="getCarouselData")
	public void addToCarouselSchedular(Hashtable<String,String> data) {

		HomePage homePage = loginToAccount();
		homePage.clickonCMS();
		homePage.clickonHomePage();
		CarouselPage carousel = homePage.clickonCaeousal();
		int initialItems = carousel.totalItemIndex();
		carousel.clickonAddImage();
		carousel.enterRedirectUrlToLastItem(data.get("redirectURL"));
		carousel.selectSchedularImageToLastItem();
		carousel.uploadSchedularImageToLastItem();
		
		carousel.selectSchedularTimeToLastItem(data.get("date"),
				data.get("duration_day"),
				data.get("duration_hours"),
				data.get("duration_minutes"),
				data.get("hours"),
				data.get("minutes"));
		
		carousel.saveItem();
		int finalItems = carousel.totalItemIndex();

		if(finalItems==(initialItems+1))
			reportTestStep(true, "Item is successfully addded in Carousel and is Scheduled");
		else
			reportTestStep(false, "Item is NOT successfully addded in Carousel and is scheduled");

	}*/

}
