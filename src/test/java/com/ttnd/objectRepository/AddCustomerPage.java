package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class AddCustomerPage extends Page{

	public AddCustomerPage(RemoteWebDriver driver){
		super(driver, firstNameLocator);
	}
	
	/*
	 * Add firstname to custome name
	 */
	public void addFirstName(String firstName){

		sendKeys(findElement(driver, firstNameLocator,10), firstName);
		TestBase.reportTestStep(true, "Enter user firstName " + firstName);

	}

	/*
	 * Add email
	 */
	public void addEmail(String email){

		sendKeys(findElement(driver, emailLocator,10), email);
		TestBase.reportTestStep(true, "Enter user email " + email);

	}
	
	/*
	 * add mobile
	 */
	public void addMobile(String mobile){

		sendKeys(findElement(driver, mobileLocator,10), mobile);
		TestBase.reportTestStep(true, "Enter user mobile " + mobile);

	}
	
	/*
	 * Add password
	 */
	public void addPassword(String mobile){

		sendKeys(findElement(driver, passwordLocator,10), mobile);
		TestBase.reportTestStep(true, "Enter user password " + passwordLocator);

	}
	
	/*
	 * Click on save 
	 */
	public CustomersPage clickSave(){

		clickElement(findElement(driver, saveLocator,10));
		TestBase.reportTestStep(true, "Clicked on Save New Customer Details");
		return new CustomersPage(driver);

	}
	
	static By firstNameLocator = By.xpath("//*[@id='firstName']");
	static By emailLocator = By.xpath("//*[@id='emailId']");
	static By passwordLocator = By.xpath("//*[@id='password']");
	static By mobileLocator = By.xpath("//*[@id='mobileNumber']");
	static By saveLocator = By.xpath("//*[@id='submit']");

}
