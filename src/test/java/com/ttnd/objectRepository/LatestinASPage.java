package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class LatestinASPage extends Page{

	public LatestinASPage(RemoteWebDriver driver){
		super(driver, addPanelLocator);

	}
	
	public int totalPanel(){
		
		int i =3;
		
		while(isElementFound(driver, By.xpath("//*[@id='homePage']/div/div["+i+"]"))){
			TestBase.reportTestStep(true, "Panel "+i+ " exists in Latest In AS");
            i++;
        }
		i = i-3;

		if(i>=1){
			TestBase.reportTestStep(true, "Total Panel : "+ i);
		}
		else{
			TestBase.reportTestStep(false, "No Panel in list");
		}
		
		return(i);
	}
	
	

	static By addPanelLocator = By.xpath("//div[@onclick='createNewPanel()']");

}
