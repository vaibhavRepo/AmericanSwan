package com.ttnd.objectRepository;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class BlogPage extends Page{

	public BlogPage(RemoteWebDriver driver){
		super(driver, BlogLocator);


	}

    /*
     * Get total number of blogs present on page
     */
	public int totalBlogIndex(){

		int i =0;

		while(isElementFound(driver, By.xpath("//*[@name='homePageBlogImages["+i+"].description']"))){
			TestBase.reportTestStep(true, "Blog "+i+ " exists in Blog Page");
			i++;
		}

		if(i>=1 && i<=5){
			TestBase.reportTestStep(true, "Total Blogs : "+ i);
		}
		else{
			TestBase.reportTestStep(false, "No Blogs Present or exceeding 5 Blogs ");
		}

		return(i);
	}

	/*
	 * Add description to blog
	 */
	public void addDescriptionToLastBlog(){

		sendKeys(findElement(driver, 
				By.xpath("//*[@name='homePageBlogImages["+(totalBlogIndex()-1)+"].description']"), 3),
				"Stone Studded Watch Is The New Statement Jewellery");
		TestBase.reportTestStep(true, "Blog Description added in last blog");

	}

	/*
	 * Add Redirect to blog
	 */
	public void addRedirectUrlToLastBlog(){

		sendKeys(findElement(driver, 
				By.xpath("//*[@name='homePageBlogImages["+(totalBlogIndex()-1)+"].redirectUrl']"), 3),
				"http://www.americanswan.com/blog");
		TestBase.reportTestStep(true, "Blog ReDirect Url added in last blog");

	}


	/*
	 * Will upload image using Auto IT tool script
	 */
	public void uploadDefaultImageToLastBlog(){
		clickElement(findElement(driver, 
				By.xpath("//*[@id='homePageBlogImages["+(totalBlogIndex()-1)+"].file']"),
				3));

		TestBase.reportTestStep(true, "Clicked on Browse Image");
		try {
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\main\\resources\\FileUpload.exe");
			TestBase.reportTestStep(true, "Upload image file successfully");

		} catch (IOException e) {
			TestBase.reportTestStep(true, "Image file not uploaded successfully");
			e.printStackTrace();
		}

	}

	/*
	 * Click on Save
	 */
	public void saveItem(){
		clickElement(findElement(driver, saveButtonLocator, 3));

		TestBase.reportTestStep(true, "Clicked on Save button");

	}

	/*
	 * Click on Add item
	 */
	public void clickonAddImage(){
		clickElement(findElement(driver, addImageLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Add Image Button");
	}

	static By BlogLocator = By.xpath("//*[@id='homePageBlogs']/div/legend");
	static By addImageLocator = By.xpath("//div[contains(text(),'Add Image')]");

	static By imageRow = By.xpath("//*[@id='table_data_table']/tbody/tr[3]");
	static By saveButtonLocator = By.xpath("//*[@id='submit']");

}
