package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class NewArrivals extends Page{

	public NewArrivals(RemoteWebDriver driver){
		super(driver, NewArrivalLocator);

	}
	
	public void expandMenProducts(){
		clickElement(findElement(driver, menproductLocator, 10));
	}
	
	public void expandWomenProducts(){
		clickElement(findElement(driver, womenproductLocator, 10));
	}
	
	public void expandDefaultProducts(){
		clickElement(findElement(driver, defaultproductLocator, 10));
	}
	
	public void clickSubmit(){
		clickElement(findElement(driver, saveLocator, 10));
		TestBase.reportTestStep(true, "Expanded products in New Arrival");
	}
	
	public void clickMenTab(){
		clickElement(findElement(driver, MenLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Men Tab in New Arrival");
	}
	
	public void clickWomenTab(){
		clickElement(findElement(driver, WomenLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Women Tab in New Arrival");
	}
	
	public void clickDefaultTab(){
		clickElement(findElement(driver, DefaultLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Default Tab in New Arrival");
	}
	
	public int gettotalProductsPresent(){
		
		int i =0;
		
		while(isElementFound(driver, By.xpath("//*[@name='homePageProductDetailsFemaleList["+i+"].productName']"))){
			TestBase.reportTestStep(true, "Product "+i+ "exists in CarouselPage");
            i++;
        }
	
		if(i>0){
			TestBase.reportTestStep(true, "Total Products : "+ i);
		}
		else{
			TestBase.reportTestStep(false, "No Products Present");
		}
		
		return(i);
	}
	
	public void deleteAllMenProductsPresent(){
		int count = 0;
		int counter= gettotalProductsPresent();
		for(int i=1; i<=counter ;i++){
			 if(isElementFound(driver, DeleteProductLocator)){
			 	clickElement(findElement(driver, DeleteProductLocator,10));
			 	try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 	clickSubmit();
			 	clickMenTab();
			 	expandMenProducts();
			 }
		}
	}
	
		public void deleteAllWomenProductsPresent(){
			int count = 0;
			int counter= gettotalProductsPresent();
			for(int i=1; i<=counter ;i++){
				 if(isElementFound(driver, DeleteProductLocator)){
				 	clickElement(findElement(driver, DeleteProductLocator,10));
				 	try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				 	clickSubmit();
				 	clickWomenTab();
				 	expandWomenProducts();
				 }
			}
		}
		
			public void deleteAllDefaultProductsPresent(){
				int count = 0;
				int counter= gettotalProductsPresent();
				for(int i=1; i<=counter ;i++){
					 if(isElementFound(driver, DeleteProductLocator)){
					 	clickElement(findElement(driver, DeleteProductLocator,10));
					 	try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					 	clickSubmit();
					 	clickDefaultTab();
					 	expandDefaultProducts();
					 }
				}
	}
	
	static By NewArrivalLocator = By.xpath("//*[@id='homePageNewArrivalsForm']/div/ul");
	static By MenLocator = By.xpath("//*[@id='menLi']/a");
	static By WomenLocator = By.xpath("//*[@id='womenLi']/a");
	//static By WomenLocator = By.xpath("//a[@onclick='loadThisSection('women', this)']");
	static By DefaultLocator = By.xpath("//*[@id='defaultLi']/a");
	//static By productLocator = By.xpath("//*[@class='accordion-heading subHeading']");
	static By menproductLocator = By.xpath("//*[@id='men']/div/div/div[1]/div[2]/div[2]/div/a/div");
	static By womenproductLocator = By.xpath("//*[@id='women']/div/div/div[1]/div[2]/div[2]/div/a/div");
	static By defaultproductLocator = By.xpath("//*[@id='default']/div/div/div[1]/div[2]/div[2]/div/a/div");
	static By DeleteProductLocator = By.xpath("//div[@onclick='deleteProductRow(this)']");
	static By saveLocator = By.xpath("//*[@id='Submit']");
}
