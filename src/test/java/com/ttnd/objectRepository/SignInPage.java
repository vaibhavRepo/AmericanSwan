package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;
import com.ttnd.utilities.Keywords;

public class SignInPage extends Page{

	public SignInPage(RemoteWebDriver driver){
		super(driver, emailLocator);

	}
	
	public void enterEmail(String email){
		sendKeys(findElement(driver, emailLocator, 10), email);
		TestBase.reportTestStep(true, "Entered user email id");
	}
	
	public void enterPassword(String password){
		sendKeys(findElement(driver, passwordLocator, 10), password);
		TestBase.reportTestStep(true, "Entered user password");

	}
	
	public HomePage clickLoginButton(){
		clickElement(findElement(driver, loginLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Login Button");
		return new HomePage(driver);
	}
	
	static By emailLocator = By.xpath("//*[@id='inputEmail']");
	static By passwordLocator = By.xpath("//*[@id='inputPassword']");
	static By loginLocator = By.xpath("//*[@type='submit']");

}
