package com.ttnd.objectRepository;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class CarouselPage extends Page{

	public CarouselPage(RemoteWebDriver driver){
		super(driver, CarouselLocator);

			
	}
	
	
	public int totalItemIndex(){
		
		int i =1;
		
		while(isElementFound(driver, By.xpath("//*[@id='table_data_table']/tbody/tr["+i+"]/td"))){
            i++;
        }
		i = i-1;

		if(i>1){
			TestBase.reportTestStep(true, "index position : "+ i);
		}
		else{
			TestBase.reportTestStep(false, "No Image present in list");
		}
		
		return(i);
	}
	
	public void enterRedirectUrlToLastItem(String redirectURL){
		sendKeys(findElement(driver, 
							By.xpath("//*[@name='homePageCarouselImages["+(totalItemIndex()-1)+"].redirectUrl']"), 3),
							redirectURL);
		TestBase.reportTestStep(true, "Enter Redirect URl in last item");
	
	}
	
	public void selectDefaultImageToLastItem(){
		clickElement(findElement(driver, 
							By.xpath("//*[@name='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularImageSelected']"),
							3));
	
		TestBase.reportTestStep(true, "Selected Default Image");
	
	}
	
	public void selectSchedularImageToLastItem(){
		clickElement(findAllElements(driver,
				By.xpath("//*[@name='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularImageSelected']"),
				8).get(1));
		
		
		
		TestBase.reportTestStep(true, "Selected Schedular Image");
	
	}
	
	public void uploadDefaultImageToLastItem(){
		clickElement(findElement(driver, 
							By.xpath("//*[@name='homePageCarouselImages["+(totalItemIndex()-1)+"].file']"),
							3));
	
		TestBase.reportTestStep(true, "Clicked on Browse Image");
		try {
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\main\\resources\\FileUpload.exe");
			TestBase.reportTestStep(true, "Upload image file successfully");

		} catch (IOException e) {
			TestBase.reportTestStep(true, "Image file not uploaded successfully");
			e.printStackTrace();
		}
	
	}
	
	public void uploadSchedularImageToLastItem(){
		
		clickElement(findAllElements(driver,
				By.xpath("//*[@name='homePageCarouselImages["+(totalItemIndex()-1)+"].file1']"),
				8).get(0));
		
		TestBase.reportTestStep(true, "Clicked on Schedular Browse Image");
		try {
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\main\\resources\\FileUpload.exe");
			TestBase.reportTestStep(true, "Upload image file successfully");

		} catch (IOException e) {
			TestBase.reportTestStep(true, "Image file not uploaded successfully");
			e.printStackTrace();
		}
	
	}
	
	public void selectSchedularTimeToLastItem(String date,
											String duration_day,
											String duration_hours,
											String duration_minutes,
											String hours,
											String minutes){
		
		clickElement(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.activationDate']"),
				3));
		
		sendKeys(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.activationDate']"),
				3),date);
		
		clickElement(findElement(driver, CarouselLocator, 3));
		
		clickElement(findElement(driver, 
				By.xpath("/*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.days']"),
			10));
		
		sendKeys(findElement(driver, 
				By.xpath("/*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.days']"),
				10),duration_day);
		
		clickElement(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.hours']"),
				3));
		
		sendKeys(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.hours']"),
				3),duration_hours);
		
		clickElement(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.minutes']"),
				3));
		
		sendKeys(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.minutes']"),
				3),duration_minutes);
		
		clickElement(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.activationHour']"),
				3));
		
		sendKeys(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.activationHour']"),
				3),hours);
		
		clickElement(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.activationMinute']"),
				3));
		
		sendKeys(findElement(driver, 
				By.xpath("//*[@id='homePageCarouselImages["+(totalItemIndex()-1)+"].schedularData.activationMinute']"),
				3),minutes);
		}
	
	public void saveItem(){
		clickElement(findElement(driver, saveButtonLocator, 3));
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		TestBase.reportTestStep(true, "Clicked on Save button");
	
	}
	
	public void clickonAddImage(){
		clickElement(findElement(driver, addImageLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Add Image Button");
	}
	
	static By CarouselLocator = By.xpath("//*[@id='homePageCarousel']/div/legend");
	static By addImageLocator = By.xpath("//div[contains(text(),'Add Image')]");
    static By imageRow = By.xpath("//*[@id='table_data_table']/tbody/tr[3]");
	static By saveButtonLocator = By.xpath("//a[contains(text(),'Save')]");

}
