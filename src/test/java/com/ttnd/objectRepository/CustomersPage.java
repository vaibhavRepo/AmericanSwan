package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class CustomersPage extends Page{

	public CustomersPage(RemoteWebDriver driver){
		super(driver, CustomersLocator);
	}
	
	public AddCustomerPage addCustomer(){
		clickElement(findElement(driver, AddCustomerLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Add Customer");
		return new AddCustomerPage(driver);
	}
	
	public void clickFilter(){
		clickElement(findElement(driver, filterLocator, 10));
		TestBase.reportTestStep(true, "Clicked on expand filter");
	}
	
	public void addEmailName(String email){

		sendKeys(findElement(driver, emailLocator,10), email);
		TestBase.reportTestStep(true, "Enter user email " + email);

	}
	
	public void clickSeach(){

		clickElement(findElement(driver, searchLocator, 10));
		TestBase.reportTestStep(true, "Clicked on search customer");
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clickAllCheckbox(){

		clickElement(findElement(driver, allCheckboxLocator, 10));
		TestBase.reportTestStep(true, "Check all checked boxes i.e. select all customers");
	}
	
	public void clickSave(){

		clickElement(findElement(driver, saveLocator, 10));
		TestBase.reportTestStep(true, "Save the user changes");

	}
	
	public void clickeditCustomerDetails(){

		clickElement(findElement(driver, editDetailsLocator, 10));
		TestBase.reportTestStep(true, "Clicked on edit cstomer detail");

	}
	
	public void clickAddressCustomerDetails(){

		clickElement(findElement(driver, editAddressLocator, 10));

	}
	
	public boolean isEmailRegistered(String email){

		if(isElementFound(driver, verifySearchLocator)){
			TestBase.reportTestStep(true, "Record found with associated email");
			return true;
		}
		TestBase.reportTestStep(true, "NO Record found with associated email");
		return false;
	}
	
	public void addCustomerAddress(String firstName,
				String lastName,
				String mobile,
				String address,
				String pincode){

		clickElement(findElement(driver, addAddressLocator, 10));
		sendKeys(findElement(driver, firstNameLocator, 10), firstName);
		sendKeys(findElement(driver, lastNameLocator, 10), lastName);
		sendKeys(findElement(driver, mobileLocator, 10), mobile);
		sendKeys(findElement(driver, addressLocator, 10), address);
		sendKeys(findElement(driver, pincodeLocator, 10), pincode);
		clickElement(findElement(driver, submitAddressLocator, 10));
	}
	
	public boolean verifyAddress(){

		if(isElementFound(driver, verifyAddressLocator)){
			return true;
		}
		return false;
	}
	
	
	static By CustomersLocator = By.xpath("//*[@id='isBlockedForOrderCreation']");
	static By AddCustomerLocator = By.xpath("//a[@class='btn btn-info']");
	static By filterLocator = By.xpath("//a[@href='#filter']");
	static By emailLocator = By.xpath("//*[@id='emailId']");
	static By searchLocator = By.xpath("//*[@id='reportSearch']");
	static By verifySearchLocator = By.xpath("//*[@id='table_data_table']/tbody/tr/td[4]");
	static By allCheckboxLocator = By.xpath("//*[@type='checkbox']");
	static By saveLocator = By.xpath("//a[@onclick='changeOrderCreationAccess()");
	static By editDetailsLocator = By.xpath("//*[@title='View & Edit']");
	static By editLocator = By.xpath("//*[@title='View & Edit']");
	static By editAddressLocator = By.xpath("//*[@id='customerInfoTabs']/li[2]/a");
	static By addAddressLocator = By.xpath("//*[@id='addressTab']/div/div/div/a");
	static By firstNameLocator = By.xpath("//*[@id='firstName']");
	static By lastNameLocator = By.xpath("//*[@id='lastName']");
	static By mobileLocator = By.xpath("//*[@id='mobileNumber']");
	static By addressLocator = By.xpath("//*[@id='addressLine1']");
	static By pincodeLocator = By.xpath("//*[@id='pinCode']");
	static By submitAddressLocator = By.xpath("//*[@id='submit']");
	static By verifyAddressLocator = By.xpath("//*[@id='addressTab']/div[2]/a");
	
	
	
	//*[@id='addressTab']/div[2]

	//*[@id='addressTab']/div[2]/a
}
