package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.ttnd.testScripts.TestBase;
import com.ttnd.utilities.Keywords;

import io.appium.java_client.MobileElement;

public class Page extends Keywords{

	protected RemoteWebDriver driver;


	public Page(RemoteWebDriver driver, By locator) {
		this.driver = driver;
		WaitForElement(driver, locator);
		
	}
	
	public void clickonCMS(){
		clickElement(findElement(driver, cmsDropDownLocator, 10));
		TestBase.reportTestStep(true, "Clicked on CMS");
	}
	
	public void clickonManage(){
		clickElement(findElement(driver, manageDropDownLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Manage");
	}
	
	public BlogPage clickonBlogs(){
		clickElement(findElement(driver, BlogsLocator, 10));
		TestBase.reportTestStep(true, "Clicked on CMS");
		return new BlogPage(driver);
	}
	
	public CustomersPage clickonCustomers(){
		clickElement(findElement(driver, CustomersLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Customers");
		return new CustomersPage(driver);
	}
	
	public void clickonHomePage(){
		clickElement(findElement(driver, HomePageLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Home Page");
	}
	
	public CarouselPage clickonCaeousal(){
		clickElement(findElement(driver, CarouselLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Carousel");
		return new CarouselPage(driver);
	}
	
	public MediaPage clickonMedia(){
		clickElement(findElement(driver, MediaLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Media");
		return new MediaPage(driver);
	}
	
	public NewArrivals clickonNewArrivals(){
		clickElement(findElement(driver, NewArrivalsLocator, 10));
		TestBase.reportTestStep(true, "Clicked on New Arrivals");
		return new NewArrivals(driver);
	}
	
	static By viewCartLocator = By.xpath("//*[@id='icon_cart']");
	static By cmsDropDownLocator = By.xpath("//*[@id='navbarCollapse']/div/ul/li[1]/a");
	static By manageDropDownLocator = By.xpath("//*[@id='navbarCollapse']/div/ul/li[2]/a");
	static By HomePageLocator = By.linkText("Home Page");
	static By CarouselLocator = By.linkText("Carousel");
	static By NewArrivalsLocator = By.linkText("New Arrivals");
	static By LatestFromASLocator = By.linkText("Latest From AS");
	static By BlogsLocator = By.linkText("Blogs");
	static By CustomersLocator = By.linkText("Customers");
	static By MediaLocator = By.linkText("Media");



}
