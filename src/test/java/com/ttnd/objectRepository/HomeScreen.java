package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.ttnd.testScripts.TestBase;
import com.ttnd.utilities.Keywords;

import io.appium.java_client.MobileElement;

public class HomeScreen extends Page {

	public HomeScreen(RemoteWebDriver driver){
		super(driver, appLogoLocator);

	}


	static By appLogoLocator = By.xpath("//*[@id='app_logo']");
	static By navigationalLocator = By.xpath("//*[@id='imageView_darwer']");

}
