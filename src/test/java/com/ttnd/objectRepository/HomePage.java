package com.ttnd.objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class HomePage extends Page{

	public HomePage(RemoteWebDriver driver){
		super(driver, logoutLocator);

	}
	
	static By logoutLocator = By.xpath("//*[@id='logOutLink']");

}
