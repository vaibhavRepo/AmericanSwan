package com.ttnd.objectRepository;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ttnd.testScripts.TestBase;

public class MediaPage extends Page{

	public MediaPage(RemoteWebDriver driver){
		super(driver, AsSeenInLocator);
	}

	public int totalAsSeenIn(){

		int i =0;
		while(isElementFound(driver, By.xpath("//*[@id='newTaxRate"+i+"']"))){
			i++;
		}

		if(i>=1){
			TestBase.reportTestStep(true, "Total As Seen In  : "+ i);
		}
		else{
			TestBase.reportTestStep(false, "No As Seen In items present in list");
		}

		return(i);
	}
	
	public void enterNameToNew(String Post){
		sendKeys(findElement(driver, 
							By.xpath("//*[@id='mediaImageDataList["+totalAsSeenIn()+"].name']"), 10),
							Post);
		TestBase.reportTestStep(true, "Added Name in As Seen In");
	
	}
	
	public void enterPostedOnToNew(String date){
		sendKeys(findElement(driver, 
							By.xpath("//*[@id='mediaImageDataList_"+totalAsSeenIn()+"_postedOn']"), 10),
							date);
		clickElement(findElement(driver, By.xpath("//*[@id='addMediaContent']/legend"), 10));
		TestBase.reportTestStep(true, "Added Posted On Date in As Seen In");
	
	}
	
	public void uploadDefaultImageToLastItem(){
		clickElement(findElement(driver, 
							By.xpath("//*[@id='mediaImageDataList["+totalAsSeenIn()+"].file']"),
							3));
	
		TestBase.reportTestStep(true, "Clicked on Browse Image");
		try {
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\main\\resources\\FileUpload.exe");
			TestBase.reportTestStep(true, "Upload image file successfully");

		} catch (IOException e) {
			TestBase.reportTestStep(true, "Image file not uploaded successfully");
			e.printStackTrace();
		}
	
	}
	
	public void clickonAdd(){
		clickElement(findElement(driver, addImageLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Add Button");
	}
	
	public void clickonSave(){
		clickElement(findElement(driver, saveLocator, 10));
		TestBase.reportTestStep(true, "Clicked on Save Button");
	}
	
	static By AsSeenInLocator = By.xpath("//*[@id='test2']/a");
	static By addImageLocator = By.xpath("//button[@onclick='createNewTaxRate()']");
	static By saveLocator = By.xpath("//*[@id='submit']");


}
